using UnityEngine;
using System;

public class DclEntityMgr : MonoBehaviour
{
	static private DclEntityMgr instance;
	private DclEntity[] entities;

	static public DclEntity GetById(int id)
	{
		if (instance == null) return null;

		return Array.Find(instance.entities, e => id == e.id );
	}

	private void Start()
	{
		instance = this;
		entities = FindObjectsOfType<DclEntity>();
	}
}
