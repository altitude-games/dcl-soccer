using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclBillboard : DclComponent
{
	public override void WriteJSON(JsonWriter writer)
	{
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("Billboard");
		}
		writer.WriteEndObject();
	}
}
