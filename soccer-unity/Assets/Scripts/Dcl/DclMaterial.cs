using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclMaterial : DclComponent
{
	public Material material;
	public bool hasAlpha = false;

	public override void WriteJSON(JsonWriter writer)
	{
#if UNITY_EDITOR
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("Material");

			writer.WritePropertyName("alpha");
			writer.WriteValue(material.color.a);

			writer.WritePropertyName("albedoColor");
			writer.WriteValue("#" + ColorUtility.ToHtmlStringRGB(material.color));

			writer.WritePropertyName("hasAlpha");
			writer.WriteValue(hasAlpha);

			if (material.mainTexture != null)
			{
				string path = AssetDatabase.GetAssetPath(material.mainTexture);

				writer.WritePropertyName("albedoTexture");
				writer.WriteValue(path);
			}
		}
		writer.WriteEndObject();
#endif
	}
}
