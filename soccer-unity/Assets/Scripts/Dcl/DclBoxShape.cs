using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclBoxShape : DclComponent
{
	public bool withCollisions = true;
	public DclMaterial dclMaterial;

	public override void WriteJSON(JsonWriter writer)
	{
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("BoxShape");

			writer.WritePropertyName("withCollisions");
			writer.WriteValue(withCollisions);

			if (dclMaterial != null)
			{
				writer.WritePropertyName("material");
				
				DclEntity e = dclMaterial.GetComponent<DclEntity>();
				writer.WriteValue(e.id);
			}

		}
		writer.WriteEndObject();
	}
}
