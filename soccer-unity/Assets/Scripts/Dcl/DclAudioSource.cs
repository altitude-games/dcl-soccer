#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclAudioSource : DclComponent
{
	public AudioClip clip;
	public DclEntity ent;
	private bool isPlay = false;

	public void Play()
	{
		Server.Instance.ForceSnapshot();
		isPlay = true;
	}

	public void WriteSnapshot(JsonWriter writer, bool isFull)
	{
		if (isPlay == false && !isFull)
			return;

		writer.WriteStartObject();

		writer.WritePropertyName("id");
		writer.WriteValue(ent.id);

		writer.WritePropertyName("isPlay");
		writer.WriteValue(isPlay);

		writer.WriteEndObject();

		isPlay = false;
	}

	public override void WriteJSON(JsonWriter writer)
	{
#if UNITY_EDITOR
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("AudioSource");

			string srcPath = AssetDatabase.GetAssetPath(clip.GetInstanceID());

			writer.WritePropertyName("clip");
			writer.WriteValue(srcPath);
		}
		writer.WriteEndObject();
#endif
	}
}
