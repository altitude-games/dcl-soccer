using UnityEngine;
using Unity.Collections;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class DclEntity : MonoBehaviour
{
	public bool isVisible = true;
	public bool sendToClients = false;
	public int id = 0;

	public bool sendPos = true;
	public bool sendRot = false;
	public bool sendScale = false;

	private bool prevActive;
	private Vector3 prevPos;
	private Vector3 prevScale;
	private Quaternion prevRot;

	public void WriteSnapshot(JsonWriter writer, bool isFull)
	{
		Vector3 pos = transform.localPosition;
		Vector3 scale = transform.localScale;
		Quaternion rot = transform.localRotation;
		bool active = gameObject.activeSelf;

		if (prevPos == pos && prevRot == rot && prevScale == scale && prevActive == active && !isFull)
			return;

		writer.WriteStartObject();
		writer.WritePropertyName("id");
		writer.WriteValue(id);

		if (sendPos && (prevPos != pos || prevActive != active || isFull))
		{
			writer.WritePropertyName("p");
			writer.WriteStartObject();
			writer.WritePropertyName("x"); writer.WriteValue(pos.x);
			writer.WritePropertyName("y"); writer.WriteValue(active == false ? pos.y-10 : pos.y);
			writer.WritePropertyName("z"); writer.WriteValue(pos.z);
			writer.WriteEndObject();
		}

		if (sendScale && (prevScale != scale || isFull))
		{
			writer.WritePropertyName("s");
			writer.WriteStartObject();
			writer.WritePropertyName("x"); writer.WriteValue(scale.x);
			writer.WritePropertyName("y"); writer.WriteValue(scale.y);
			writer.WritePropertyName("z"); writer.WriteValue(scale.z);
			writer.WriteEndObject();
		}

		if (sendRot && (prevRot != rot || isFull))
		{
			writer.WritePropertyName("r");
			writer.WriteStartObject();
			writer.WritePropertyName("x"); writer.WriteValue(rot.x);
			writer.WritePropertyName("y"); writer.WriteValue(rot.y);
			writer.WritePropertyName("z"); writer.WriteValue(rot.z);
			writer.WritePropertyName("w"); writer.WriteValue(rot.w);
			writer.WriteEndObject();
		}

		prevPos = pos;
		prevScale = scale;
		prevRot = rot;
		prevActive = active;

		writer.WriteEndObject();
	}

	public void WriteJSON(JsonWriter writer)
	{
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("Entity");
			
			writer.WritePropertyName("name");
			writer.WriteValue(name);

			writer.WritePropertyName("id");
			writer.WriteValue(id);

			writer.WritePropertyName("isVisible");
			writer.WriteValue(isVisible);

			WriteTransform(writer);

			writer.WritePropertyName("children");
			writer.WriteStartArray();

			foreach (Transform child in transform)
			{
				DclEntity e = child.GetComponent<DclEntity>();
				if (e == null) continue;
				if (e.gameObject.activeInHierarchy == false) continue;

				e.WriteJSON(writer);
			}
			writer.WriteEndArray();

			writer.WritePropertyName("components");
			writer.WriteStartArray();

			DclComponent[] components = GetComponents<DclComponent>();
			foreach (DclComponent c in components)
			{
				if (c == this) continue;
				c.WriteJSON(writer);
			}
			writer.WriteEndArray();
		}
		writer.WriteEndObject();
	}

	private void WriteTransform(JsonWriter writer)
	{
		Vector3 pos = transform.localPosition;
		Vector3 scale = transform.localScale;
		Quaternion rot = transform.localRotation;

		writer.WritePropertyName("p");
		writer.WriteStartObject();
		writer.WritePropertyName("x"); writer.WriteValue(pos.x);
		writer.WritePropertyName("y"); writer.WriteValue(pos.y);
		writer.WritePropertyName("z"); writer.WriteValue(pos.z);
		writer.WriteEndObject();

		writer.WritePropertyName("s");
		writer.WriteStartObject();
		writer.WritePropertyName("x"); writer.WriteValue(scale.x);
		writer.WritePropertyName("y"); writer.WriteValue(scale.y);
		writer.WritePropertyName("z"); writer.WriteValue(scale.z);
		writer.WriteEndObject();

		writer.WritePropertyName("r");
		writer.WriteStartObject();
		writer.WritePropertyName("x"); writer.WriteValue(rot.x);
		writer.WritePropertyName("y"); writer.WriteValue(rot.y);
		writer.WritePropertyName("z"); writer.WriteValue(rot.z);
		writer.WritePropertyName("w"); writer.WriteValue(rot.w);
		writer.WriteEndObject();
	}
}
