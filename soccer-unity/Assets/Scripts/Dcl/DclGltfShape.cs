using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclGltfShape : DclComponent
{
	public Object gltfAsset;

	public override void WriteJSON(JsonWriter writer)
	{
#if UNITY_EDITOR
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("GLTFShape");

			string srcPath = AssetDatabase.GetAssetPath(gltfAsset.GetInstanceID());

			writer.WritePropertyName("src");
			writer.WriteValue(srcPath);
		}
		writer.WriteEndObject();
#endif
	}
}
