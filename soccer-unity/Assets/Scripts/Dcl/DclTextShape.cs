using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using TMPro;

public class DclTextShape : DclComponent
{
	const float multScale = 10;
	const float multFontSize = 1.0f;

	public int lineCount = 1;
	public bool sendToClients = false;

	private TextMeshPro text;
	private RectTransform rt;
	private DclEntity ent;

	private string textValue;

	private string prevValue;

	public void SetText(string s)
	{
		if (text != null)
			text.SetText(s);

		textValue = s;
	}

	public void WriteSnapshot(JsonWriter writer, bool isFull)
	{
		if (prevValue == textValue && !isFull)
			return;

		DclEntity e = GetComponent<DclEntity>();

		writer.WriteStartObject();
		writer.WritePropertyName("id");
		writer.WriteValue(ent.id);

		writer.WritePropertyName("val");
		writer.WriteValue(textValue);

		writer.WriteEndObject();

		prevValue = textValue;
	}

	public override void WriteJSON(JsonWriter writer)
	{
		if (writer == null)
			return;

		//Debug.LogFormat("Writing TMP3D {0}", gameObject.GetHierachyPath());

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("TextShape");
			WriteText(writer);
		}
		writer.WriteEndObject();
	}

	private void Start()
	{
		text = GetComponent<TextMeshPro>();
		rt = GetComponent<RectTransform>();
		ent = GetComponent<DclEntity>();
	}

	private void WriteText(JsonWriter writer)
	{
		text = GetComponent<TextMeshPro>();
		if (text == null) 
			return;

		writer.WritePropertyName("value");
		writer.WriteValue(text.text);

		writer.WritePropertyName("fontSize");
		writer.WriteValue(text.fontSize * multFontSize);

		if (text.font != null)
		{
			string fontName = text.font.name;
			if (text.font.name == "LiberationSans SDF")
			{
				fontName = "Arial";
			}
			writer.WritePropertyName("fontFamily");
			writer.WriteValue(fontName);
		}


		writer.WritePropertyName("lineCount");
		writer.WriteValue(lineCount);

		rt = GetComponent<RectTransform>();
		if (rt != null)
		{
			writer.WritePropertyName("width");
			writer.WriteValue(rt.rect.width / multScale);

			writer.WritePropertyName("height");
			writer.WriteValue(rt.rect.height / multScale);
		}

		writer.WritePropertyName("color");
		writer.WriteValue("#" + ColorUtility.ToHtmlStringRGB(text.color));

		writer.WritePropertyName("vAlign");
		writer.WriteValue(GetVAlign(text));

		writer.WritePropertyName("hAlign");
		writer.WriteValue(GetHAlign(text));

		WriteOutlineProperties(text, writer);
		WriteShadowProperties(text, writer);
	}

	private string GetVAlign(TextMeshPro text)
	{
		if (text == null) return "center";

		switch(text.alignment)
		{
			case TextAlignmentOptions.Bottom:
			case TextAlignmentOptions.BottomLeft:
			case TextAlignmentOptions.BottomRight:
			case TextAlignmentOptions.BottomFlush:
			case TextAlignmentOptions.BottomGeoAligned:
			case TextAlignmentOptions.BottomJustified:
				return "bottom";

			case TextAlignmentOptions.Top:
			case TextAlignmentOptions.TopLeft:
			case TextAlignmentOptions.TopRight:
			case TextAlignmentOptions.TopFlush:
			case TextAlignmentOptions.TopGeoAligned:
			case TextAlignmentOptions.TopJustified:
				return "top";
		}

		return "center";
	}

	private string GetHAlign(TextMeshPro text)
	{
		if (text == null) return "center";

		switch(text.alignment)
		{
			case TextAlignmentOptions.Left:
			case TextAlignmentOptions.TopLeft:
			case TextAlignmentOptions.TopFlush:
			case TextAlignmentOptions.BottomLeft:
			case TextAlignmentOptions.BottomJustified:
				return "left";

			case TextAlignmentOptions.Right:
			case TextAlignmentOptions.BottomRight:
			case TextAlignmentOptions.TopRight:
				return "right";
		}

		return "center";
	}

	[ContextMenu("stuff")]
	public void Test()
	{
		TextMeshPro text = GetComponent<TextMeshPro>();
	}

	public void WriteOutlineProperties(TextMeshPro text, JsonWriter writer)
	{
		if (text == null || writer == null)
			return;

		const float mult = 5.0f * multFontSize;

		Material mat = text.fontSharedMaterial;
		if (mat == null)
		{
			mat = text.fontMaterial;
			if (mat == null)
			{
				Debug.Log("Cannot find material", this);
				return;
			}
		}

		writer.WritePropertyName("outlineWidth");
		writer.WriteValue(mat.GetFloat("_OutlineWidth") * mult);

		writer.WritePropertyName("outlineColor");
		writer.WriteValue("#" + ColorUtility.ToHtmlStringRGB(mat.GetColor("_OutlineColor")));
	}

	public void WriteShadowProperties(TextMeshPro text, JsonWriter writer)
	{
		if (text == null || writer == null)
			return;

		const float mult = 2.0f * multFontSize;

		Material mat = text.fontMaterial;

		writer.WritePropertyName("shadowOffsetX");
		writer.WriteValue(mat.GetFloat("_UnderlayOffsetX") * mult);

		writer.WritePropertyName("shadowOffsetY");
		writer.WriteValue(mat.GetFloat("_UnderlayOffsetY") * -mult);

		writer.WritePropertyName("shadowBlur");
		writer.WriteValue(mat.GetFloat("_UnderlaySoftness") * 20);

		writer.WritePropertyName("shadowColor");
		writer.WriteValue("#" + ColorUtility.ToHtmlStringRGB(mat.GetColor("_UnderlayColor")));
	}
}
