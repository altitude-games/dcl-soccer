using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;

public class DclOnClick : DclComponent
{
	public string evtName = "";
	public string param = "";
	public override void WriteJSON(JsonWriter writer)
	{
		if (writer == null)
			return;

		writer.WriteStartObject();
		{
			writer.WritePropertyName("tag");
			writer.WriteValue("OnClick");

			writer.WritePropertyName("evtName");
			writer.WriteValue(evtName);

			writer.WritePropertyName("param");
			writer.WriteValue(param);
		}
		writer.WriteEndObject();
	}
}
