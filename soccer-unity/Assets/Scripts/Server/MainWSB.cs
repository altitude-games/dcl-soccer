using UnityEngine;
using System;
using System.Collections.Generic;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;


public class MainWSB : WebSocketBehavior
{
	static private int nextId = 0;
	static public List<MainWSB> Clients = new List<MainWSB>();

	public int Id;
	public GameObject Player = null;

	public MainWSB() : base()
	{
		Debug.Log("new instance of MainWSB");
	}

	protected override void OnOpen()
	{
		base.OnOpen();
		Clients.Add(this);
		Id = nextId++;

		Server.Instance.OnNewClient(this);
		Debug.LogFormat("OnOpen: Client {0}", Id);
	}

	protected override void OnMessage (MessageEventArgs e)
	{
		JObject message = JObject.Parse(e.Data);
		if (message == null)
			return;

		string method = (string)message["method"];
		if (method == EventType.ClickTable.ToString())
		{
			ServerEventQ.Instance.Enqueue(EventType.ClickTable, message, Id);
		}
		else if (method == EventType.Join.ToString())
		{
			ServerEventQ.Instance.Enqueue(EventType.Join, message, Id);
		}
	}

	public void SendSnapshot(string snapshot)
	{
		if (ConnectionState != WebSocketState.Open)
			return;

		Send(snapshot);
	}
}

