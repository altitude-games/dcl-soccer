using UnityEngine;
using System;
using System.Collections.Generic;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

public class EventJoin : EventHandler
{
	public Table TableRef;
	public override void Process(ServerEvent evt)
	{
		string id = evt.Json["data"]["id"].ToString();
		string param = evt.Json["data"]["param"].ToString();

		TableRef.OnJoin(param, evt.ClientId);
	}
}
