using UnityEngine;
using System;
using System.Collections.Generic;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

public enum EventType
{
	ClickTable,
	Join,
}

public class ServerEvent
{
	public int ClientId;
	public EventType Type;
	public JObject Json;
}


public class ServerEventQ : MonoBehaviour
{
	static private ServerEventQ instance;
	static public ServerEventQ Instance
	{
		get { return instance; }
	}

	private Queue<ServerEvent> queue = new Queue<ServerEvent>();
	private DclEntity[] entities;

	public EventHandler[] handlers;

	public void Enqueue(EventType type, JObject json, int clientId)
	{
		ServerEvent e = new ServerEvent() { Type = type, Json = json, ClientId = clientId };
		queue.Enqueue(e);
	}

	private void Start()
	{
		instance = this;
		entities = GameObject.FindObjectsOfType<DclEntity>();
	}

	private void Update()
	{
		while (queue.Count > 0)
		{
			ServerEvent evt = queue.Dequeue();
			Process(evt);
		}
	}

	private void Process(ServerEvent evt)
	{
		if (evt == null) return;

		int index = (int)evt.Type;
		if (index >= handlers.Length) return;
		if (handlers[index] == null) return;

		handlers[index].Process(evt);
	}
}
