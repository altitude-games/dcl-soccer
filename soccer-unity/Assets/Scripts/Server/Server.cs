using UnityEngine;
using System;
using System.Collections.Generic;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

public class Server : MonoBehaviour
{
	static public Server Instance;

	public int Port = 8888;
	public int UpdatesPerSecond = 30;
	public GameObject PlayerPrefab;

	public Table TableRef;
	public DclEntity[] SnapshotEntities;
	public DclTextShape[] SnapshotTexts;
	public DclAudioSource[] SnapshotAudio;

	private StringBuilder sb = new StringBuilder(1024 * 1024);
	private StringWriter sw;
	private WebSocketServer wssv;
	private float updateInterval;
	private float updateElapsed;
	private int snapshotId = 0;
	private bool isForceSnapshot = false;

	private List<MainWSB> toInit = new List<MainWSB>();

	public void Start()
	{
		Instance = this;
		wssv = new WebSocketServer (Port);
		wssv.AddWebSocketService<MainWSB>("/");
		wssv.Start ();
		Debug.Log("Start server");

		updateInterval = 1.0f / (float)UpdatesPerSecond;
		updateElapsed = 0;

		Application.targetFrameRate = 30;
	}

	public void ForceSnapshot()
	{
		isForceSnapshot = true;
	}

	public void OnNewClient(MainWSB wsb)
	{
		toInit.Add(wsb);
	}

	private void OnDestroy()
	{
		wssv.Stop();
	}

	private string CreateSnapshot()
	{
		sb.Clear();
		sw = new StringWriter(sb);
		bool isFull = (snapshotId % 20 == 0);
		using (JsonWriter writer = new JsonTextWriter(sw))
		{
			writer.WriteStartObject();

			writer.WritePropertyName("method");
			writer.WriteValue("snap");

			writer.WritePropertyName("id");
			writer.WriteValue(snapshotId++);

			//writer.WritePropertyName("t");
			//writer.WriteValue(Time.time);

			//TableRef.WriteSnapshot(writer);

			writer.WritePropertyName("transforms");
			writer.WriteStartArray();
			int len = SnapshotEntities.Length;
			for (int i=0; i<len; i++)
			{
				DclEntity e = SnapshotEntities[i];
				e.WriteSnapshot(writer, isFull);
			}
			writer.WriteEndArray();

			writer.WritePropertyName("texts");
			writer.WriteStartArray();
			len = SnapshotTexts.Length;
			for (int i=0; i<len; i++)
			{
				DclTextShape t = SnapshotTexts[i];
				t.WriteSnapshot(writer, isFull);
			}
			writer.WriteEndArray();

			writer.WritePropertyName("audio");
			writer.WriteStartArray();
			len = SnapshotAudio.Length;
			for (int i=0; i<len; i++)
			{
				DclAudioSource a = SnapshotAudio[i];
				a.WriteSnapshot(writer, isFull);
			}
			writer.WriteEndArray();

			writer.WriteEndObject();
		}

		return sb.ToString();
	}

	private void UpdateSnapshot()
	{
		if (MainWSB.Clients.Count <= 0)
			return;

		updateElapsed += Time.deltaTime;
		if (updateElapsed < updateInterval && !isForceSnapshot)
			return;

		string snapshot = CreateSnapshot();
		int len = MainWSB.Clients.Count;
		for (int i=0; i<len; i++)
		{
			MainWSB.Clients[i].SendSnapshot(snapshot);
		}

		updateElapsed -= updateInterval;

		if (updateElapsed < 0)
			updateElapsed = 0;
	}

	private void Update()
	{
		UpdateSnapshot();
	}
}
