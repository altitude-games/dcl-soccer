using UnityEngine;
using System;
using System.Collections.Generic;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

public class EventClickTable : EventHandler
{
	public Table TableRef;
	public float yOffset = 2.4f;

	public override void Process(ServerEvent evt)
	{
		int id = (int)evt.Json["data"]["id"];

		var cPos = evt.Json["data"]["cPos"];
		var cRot = evt.Json["data"]["cRot"];

		Vector3 pos = new Vector3();
		pos.x = (float)cPos["x"];
		pos.y = (float)cPos["y"] + yOffset;
		pos.z = (float)cPos["z"];

		Quaternion rot = new Quaternion();
		rot.x = (float)cRot["x"];
		rot.y = (float)cRot["y"];
		rot.z = (float)cRot["z"];
		rot.w = (float)cRot["w"];

		transform.position = pos;
		transform.rotation = rot;
		//Debug.LogFormat("pos {0}, rot {1}", pos, rot);

		Ray ray = new Ray(pos, transform.forward);
		RaycastHit hitInfo = new RaycastHit();
		if (Physics.Raycast(ray, out hitInfo, 20, LayerMask.GetMask("ClickTable")))
		{
			//Debug.LogFormat("hitInfo {0}", hitInfo.point);
			TableRef.OnClick(hitInfo.point, evt.ClientId);
		}
	}
}
