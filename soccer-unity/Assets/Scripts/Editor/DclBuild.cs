using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class DclBuild 
{
	static public string[] folders = new string[]
	{
		"Models",
		"Sounds",
	};

	[MenuItem("Altitude/Assign IDs")]
	public static void AssignIds()
	{
		DclEntity[] entities = GameObject.FindObjectsOfType<DclEntity>();

		int len = entities.Length;
		for (int i=0; i<len; i++)
		{
			DclEntity e = entities[i];
			e.id = i;
			EditorUtility.SetDirty(e);
		}

		EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());

	}

	[MenuItem("Altitude/Check IDs")]
	public static void CheckIds()
	{
		DclEntity[] entities = GameObject.FindObjectsOfType<DclEntity>();
		System.Array.Sort(entities, delegate(DclEntity e1, DclEntity e2) 
		{
			return e1.id - e2.id;
		});

		List<DclEntity> listCollision = new List<DclEntity>();
		int len = entities.Length;
		for (int i=1; i<len; i++)
		{
			DclEntity e1 = entities[i-1];
			DclEntity e2 = entities[i];

			if (e1.id == e2.id)
			{
				Debug.LogFormat("Duplicate ID {0} and {1}", e1.name, e2.name);
				listCollision.Add(e2);
			}
		}

		int max = entities[len-1].id + 1;
		for (int i=0; i<listCollision.Count; i++)
		{
			DclEntity e = listCollision[i];
			e.id = max++;
			EditorUtility.SetDirty(e);
		}

		if (listCollision.Count > 0)
		{
			EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
		}
	}

	[MenuItem("Altitude/Dcl Build")]
	public static void Build()
	{
		//ProcessButtonGroups();
		Debug.Log("DCL Build start");

		string srcAssets = GetLocalAssetsPath();
		string dir = GetClientAssetsPath();
		Directory.CreateDirectory(dir);

		foreach (string f in folders)
		{
			string src = Path.Combine(srcAssets, f);
			string dst = Path.Combine(dir, f);
			CopyDirectoryTree(src, dst);
			//Debug.Log("Copy tree : " + src + " to " + dst);
		}

		WriteJSON();
		Debug.Log("DCL Build done");
	}

	private static void WriteJSON()
	{
		StringBuilder sb = new StringBuilder(1024 * 1024);
		StringWriter sw = new StringWriter(sb);

		using (JsonWriter writer = new JsonTextWriter(sw))
		{
			writer.WriteStartObject();
			writer.WritePropertyName("scene");
			writer.WriteStartArray();

			int count = EditorSceneManager.loadedSceneCount;
			for (int i=0; i<count; i++)
			{
				Scene s = SceneManager.GetSceneAt(i);

				foreach (GameObject go in s.GetRootGameObjects())
				{
					DclEntity ent = go.GetComponent<DclEntity>();
					if (ent == null) continue;
					if (go.activeInHierarchy == false) continue;

					ent.WriteJSON(writer);
				}
			}

			writer.WriteEndArray();
			writer.WriteEndObject();
		}

		//System.IO.File.WriteAllText(@"../Assets/Scene.json", sb.ToString());
		
		sb.Insert(0, "export function getSceneData() { return ");
		sb.Append("; }");

		System.IO.File.WriteAllText(@"../soccer-dcl/src/scene.ts", sb.ToString());
	}

	[MenuItem("Altitude/Dcl Clean")]
	public static void Clean()
	{
		string assets = GetClientAssetsPath();
		if (Directory.Exists(assets) == false)
			return;

		System.IO.DirectoryInfo di = new DirectoryInfo(assets);

		foreach (FileInfo file in di.GetFiles())
		{
			if (file.Name.IndexOf("deploy") == 0 && Path.GetExtension(file.Name) == ".sh")
				continue;

			file.Delete(); 
		}
		foreach (DirectoryInfo dir in di.GetDirectories())
		{
			dir.Delete(true); 
		}
	}

	public static string GetClientAssetsPath()
	{
		string parent = Directory.GetParent(Directory.GetCurrentDirectory()).ToString();
		string path = Path.Combine(parent, "soccer-dcl");
		return Path.Combine(path, "assets");
	}

	public static string GetLocalAssetsPath()
	{
		string parent = Directory.GetCurrentDirectory();
		string path = Path.Combine(parent, "Assets");
		return path;
	}

	private static void CopyDirectoryTree(string srcPath, string dstPath)
	{
		Directory.CreateDirectory(dstPath);

		//Now Create all of the directories
		foreach (string dirPath in Directory.GetDirectories(srcPath, "*", SearchOption.AllDirectories))
		{
			//Debug.LogFormat("Create dir {0}", dstPath);
			Directory.CreateDirectory(dirPath.Replace(srcPath, dstPath));
		}

		string[] filters = new string[]
		{
			"*.gltf",
			"*.glb",
			"*.bin",
			"*.png",
			"*.jpg",
			"*.mp3",
		};

		foreach (string f in filters)
		{
			foreach (string newPath in Directory.GetFiles(srcPath, f, SearchOption.AllDirectories))
			{
				string src = newPath;
				string dst = newPath.Replace(srcPath, dstPath);
				File.Copy(src, dst, true);
				//Debug.LogFormat("Copy {0} to {1}", src, dst);
			}
		}
	}

	[MenuItem("Altitude/Linux Build")]
	public static void LinuxBuild()
	{
		BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
		buildPlayerOptions.scenes = new[] { "Assets/Scenes/AirHockey.unity" };

        buildPlayerOptions.locationPathName = "../build/server/air-hockey-server.x86_64";
        buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
        buildPlayerOptions.options = BuildOptions.EnableHeadlessMode;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
        }
	}

//	private static void ProcessButtonGroups()
//	{
//		ECSCompButtonGroup[] groups = Object.FindObjectsOfType<ECSCompButtonGroup>();
//		foreach(ECSCompButtonGroup g in groups)
//		{
//			if (g == null) continue;
//			g.buttons.Clear();
//		}
//
//
//		ECSCompClickable[] buttons = Object.FindObjectsOfType<ECSCompClickable>();
//		foreach(ECSCompClickable b in buttons)
//		{
//			if (b == null || b.group == null) continue;
//
//			b.group.buttons.Add(b);
//		}
//
//	}
}
