using UnityEngine;

public enum AIState
{
	Defend,
	Attack,
	Wait,
}

public class Paddle : MonoBehaviour
{
	public float speed = 10.0f;
	public Bounds bounds;
	public float forceMult = 10;
	public float aiInterval = 0.5f;

	private Rigidbody rb;
	private Vector3 dest;
	private bool move = false;

	private GameObject puck;
	private float speedCurr;

	private bool isAI = false;
	private AIState aiState;
	private float aiCurrDuration = 0;
	private Bounds boundsPuck;

	public void SetIsAI(bool ai, GameObject p)
	{
		isAI = ai;
		puck = p;

		if (ai) speedCurr = speed * 0.5f;
		else speedCurr = speed;
	}

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		dest = transform.position;
		boundsPuck = new Bounds(bounds.center, bounds.size);
		boundsPuck.Expand(0.5f);
	}

	private void Update()
	{
		if (isAI == false) return;

		aiCurrDuration -= Time.deltaTime;
		if (aiCurrDuration > 0) return;

		Vector3 puckPos = puck.transform.position;
		if (boundsPuck.Contains(puckPos) == false) 
		{
			SetDestination(GetCenter());
			aiCurrDuration = 0;
			return;
		}

		SetDestination(puckPos);
		aiCurrDuration = aiInterval;
	}

	private void FixedUpdate()
	{
		if (move == false)
			return;

		Vector3 pos = transform.position;
		Vector3 delta = dest - pos;
		float magnitude = delta.magnitude;
		if (magnitude < 0.01)
		{
			move = false;
			return;
		}

		float mult = speedCurr * Time.fixedDeltaTime;
		Vector3 newPos;

		if (magnitude < mult)
		{
			newPos = dest;
		}
		else
		{
			newPos = pos + (delta.normalized * mult);
		}

		if (bounds.Contains(newPos) == false)
		{
			newPos = bounds.ClosestPoint(newPos);
			move = false;
		}

		rb.MovePosition(newPos);
	}

	public void SetDestination(Vector3 d)
	{
		dest = d;
		move = true;
	}

	private void OnCollisionExit(Collision col)
	{
		if (isAI && col.collider.name == "Puck")
		{
			SetDestination(GetCenter());
			aiCurrDuration = aiInterval;
		}
	}

	void OnDrawGizmosSelected()
    {
        // Draw a yellow cube at the transform position
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

	private Vector3 GetCenter()
	{
		Vector3 pos = bounds.center;
		pos.y = transform.position.y;
		return pos;
	}
}
