using UnityEngine;

public class Puck : MonoBehaviour
{
	public Table TableRef;

	private Rigidbody rb;
	private DclAudioSource source;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		source = GetComponent<DclAudioSource>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.name == "GoalP1")
		{
			rb.velocity = Vector3.zero;
			TableRef.OnGoal(1);
		}
		else if (other.name == "GoalP2")
		{
			rb.velocity = Vector3.zero;
			TableRef.OnGoal(2);
		}
	}

	private void OnCollisionExit(Collision col)
	{
		source.Play();
	}
}
