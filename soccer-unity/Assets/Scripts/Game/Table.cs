﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using TMPro;


public enum TableState
{
	NotStarted,
	Wait,
	ScoredP1,
	ScoredP2,
	Playing,
	Ended
}

[System.Serializable]
public class TableData
{
	public const float WAIT_TIME = 15;
	public const float GAME_TIME = 2 * 60;

	public TableState State;
	public int IdP1 = -1;
	public int IdP2 = -1;
	public int ScoreP1 = 0;
	public int ScoreP2 = 0;
	public float Time = 0;

	public void Reset()
	{
		State = TableState.NotStarted;
		IdP1 = -1;
		IdP2 = -1;
		ScoreP1 = 0;
		ScoreP2 = 0;
		Time = 0;
	}
}

public class Table : MonoBehaviour 
{
	public Paddle Paddle1;
	public Paddle Paddle2;
	public GameObject Puck;
	public Transform StartP1;
	public Transform StartP1Puck;
	public Transform StartP2;
	public Transform StartP2Puck;
	public GameObject ButtonJoinP1;
	public GameObject ButtonJoinP2;
	public ScoreBoard ScoreBoard;

	public DclTextShape textMessage;
	public DclTextShape textScoreP1;
	public DclTextShape textScoreP2;
	public DclTextShape textTime;
	public DclTextShape textScoreMessage;

	[TextArea]
	public string msgNotStarted = "Click the\njoin buttons\nto start";

	[TextArea]
	public string msgWait = "Game starts in\n{0}\nseconds";

	private TableData data = new TableData();

	private Rigidbody rbPuck;

	public void WriteSnapshot(JsonWriter writer)
	{
		writer.WritePropertyName("table");
		writer.WriteRawValue(JsonConvert.SerializeObject(data));
	}

	public void OnScoreBoardAnimDone(string stateName)
	{
		TableState stateDone;
		System.Enum.TryParse(stateName, out stateDone);
		switch (stateDone)
		{
			case TableState.ScoredP1:
			case TableState.ScoredP2:
				SetStatePlaying();
				break;

			case TableState.Ended:
				ResetObjectsState();
				data.Reset();
				ScoreBoard.SetState(data.State);
				break;
		}
	}

	public void OnGoal(int playerWhoScored)
	{
		if (playerWhoScored == 1)
		{
			data.ScoreP1++;
			SetStateScoredP1();
		}
		else if (playerWhoScored == 2)
		{
			data.ScoreP2++;
			SetStateScoredP2();
		}
	}

	// TODO: clean up
	public void OnJoin(string param, int clientId)
	{
		if (param == "1" && data.IdP1 < 0 && data.IdP2 != clientId)
		{
			ButtonJoinP1.SetActive(false);
			data.IdP1 = clientId;
			if (data.State == TableState.NotStarted)
				SetStateWait();
		}
		else if (param == "2" && data.IdP2 < 0 && data.IdP1 != clientId)
		{
			ButtonJoinP2.SetActive(false);
			data.IdP2 = clientId;
			if (data.State == TableState.NotStarted)
				SetStateWait();
		}

		if (data.IdP1 >= 0 && data.IdP2 >= 0)
		{
			data.Time = 0;
		}
	}

	// TODO: clean up
	public void OnClick(Vector3 dest, int clientId)
	{
		//Debug.LogFormat("OnClick {0}, {1}", entityId, clientId);

		if (clientId == data.IdP1)
		{
			Paddle1.SetDestination(dest);
		}
		else if (clientId == data.IdP2)
		{
			Paddle2.SetDestination(dest);
		}
	}

	private void Start()
	{
		rbPuck = Puck.GetComponent<Rigidbody>();
		ResetObjectsState();
		ScoreBoard.SetState(data.State);
	}

	private void Update()
	{
		switch (data.State)
		{
			case TableState.NotStarted: UpdateNotStarted(); break;
			case TableState.Wait: 		UpdateWait(); 		break;
			case TableState.ScoredP1: 	UpdateScoredP1(); 	break;
			case TableState.ScoredP2: 	UpdateScoredP2(); 	break;
			case TableState.Playing: 	UpdatePlaying(); 	break;
			case TableState.Ended: 		UpdateEnded(); 		break;
		}
	}

	private void UpdateNotStarted()
	{
		textMessage.SetText(msgNotStarted);
	}

	private void UpdateWait()
	{
		data.Time -= Time.deltaTime;
		if (data.Time <= 0)
		{
			bool isP1Start = true;

			if (data.IdP1 >= 0 && data.IdP2 >= 0)
			{
				isP1Start = (Random.Range(1,3) == 1);
			}
			else
			{
				isP1Start = (data.IdP1 >= 0);
			}

			InitPlaying(isP1Start);

			data.Time = TableData.GAME_TIME;

			Paddle1.SetIsAI(data.IdP1 < 0, Puck);
			Paddle2.SetIsAI(data.IdP2 < 0, Puck);
		}

		string msg = string.Format(msgWait,Mathf.Ceil(data.Time));
		textMessage.SetText(msg);
	}

	private void InitPlaying(bool isP1Start)
	{
		SetPaddlesStart();
		if (rbPuck != null)
		{
			Vector3 pos = StartP1Puck.position;
			if (!isP1Start)
				pos = StartP2Puck.position;

			rbPuck.MovePosition(pos);
			rbPuck.velocity = Vector3.zero;
		}

		Paddle1.enabled = true;
		Paddle2.enabled = true;
		ButtonJoinP1.SetActive(false);
		ButtonJoinP2.SetActive(false);
		SetStatePlaying();
	}

	private void UpdateScoredP1()
	{
	}

	private void UpdateScoredP2()
	{
	}

	private void UpdatePlaying()
	{
		UpdatePlayingTime();

		if (data.Time <= 0)
			SetStateEnded();
	}

	private void UpdateEnded()
	{
	}

	private void SetStateWait()
	{
		data.State = TableState.Wait;
		data.Time = TableData.WAIT_TIME;
		ScoreBoard.SetState(data.State);
	}

	private void SetStatePlaying()
	{
		Debug.Log("set state to playing");
		data.State = TableState.Playing;
		ScoreBoard.SetState(data.State);

		Paddle1.enabled = true;
		Paddle2.enabled = true;
	}

	private void SetStateScoredP1()
	{
		data.State = TableState.ScoredP1;
		ScoreBoard.SetState(data.State);
		textScoreMessage.SetText("GREEN SCORES!");

		SetPaddlesStart();

		if (rbPuck != null)
		{
			rbPuck.MovePosition(StartP2Puck.position);
			rbPuck.velocity = Vector3.zero;
			rbPuck.angularVelocity = Vector3.zero;
		}
	}

	private void SetStateScoredP2()
	{
		data.State = TableState.ScoredP2;
		ScoreBoard.SetState(data.State);
		textScoreMessage.SetText("BLUE SCORES!");

		SetPaddlesStart();

		if (rbPuck != null)
		{
			rbPuck.MovePosition(StartP1Puck.position);
			rbPuck.velocity = Vector3.zero;
			rbPuck.angularVelocity = Vector3.zero;
		}
	}

	private void SetStateEnded()
	{
		data.State = TableState.Ended;
		ScoreBoard.SetState(TableState.Ended);

		rbPuck.velocity = Vector3.zero;
		rbPuck.angularVelocity = Vector3.zero;
		Paddle1.enabled = false;
		Paddle2.enabled = false;

		if (data.ScoreP1 > data.ScoreP2)
		{
			textMessage.SetText("GREEN WINS!");
		}
		else if (data.ScoreP2 > data.ScoreP1)
		{
			textMessage.SetText("BLUE WINS!");
		}
		else
		{
			textMessage.SetText("IT'S A TIE!");
		}
	}


	private void ResetObjectsState()
	{
		Paddle1.enabled = false;
		Paddle2.enabled = false;

		Paddle1.gameObject.SetActive(false);
		Paddle2.gameObject.SetActive(false);
		Puck.gameObject.SetActive(false);

		ButtonJoinP1.SetActive(true);
		ButtonJoinP2.SetActive(true);

		//data.Reset();
	}

	private void SetPaddlesStart()
	{
		Paddle1.gameObject.SetActive(true);
		Paddle1.transform.position = StartP1.position;
		Paddle1.enabled = false;

		Paddle2.gameObject.SetActive(true);
		Paddle2.transform.position = StartP2.position;
		Paddle2.enabled = false;

		Puck.gameObject.SetActive(true);

		textScoreP1.SetText(data.ScoreP1.ToString());
		textScoreP2.SetText(data.ScoreP2.ToString());
	}

	private void UpdatePlayingTime()
	{
		data.Time -= Time.deltaTime;
		int totalSecs = (int)Mathf.Ceil(data.Time);
		int secs = totalSecs % 60;
		int mins = totalSecs / 60;

		string msg = string.Format("{0}:{1:00}", mins, secs);
		textTime.SetText(msg);
	}
}
