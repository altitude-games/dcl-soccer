using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
	public Table TableRef;

	private Animator animator;

	public void OnAnimDone(string animName)
	{
		TableRef.OnScoreBoardAnimDone(animName);
	}

	public void SetState(TableState state)
	{
		if (animator != null) animator.SetTrigger(state.ToString());
	}

	private void Start()
	{
		animator = GetComponent<Animator>();
	}
}
