using UnityEngine;


public class SoccerBall : MonoBehaviour
{
	public Vector3 force;
	public float interval = 2;

	private float elapsed = 0;

	private Rigidbody rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		elapsed += Time.deltaTime;
		if (elapsed >= interval)
		{
			rb.AddForce(force);
			elapsed -= interval;
		}
	}
}
