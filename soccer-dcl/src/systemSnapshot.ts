import { Manager } from "./manager"

export class SystemSnapshot implements ISystem 
{
	snapshots = [];
	snapshotTime = 0;

	push(snap:any)
	{
		this.snapshots.push(snap);

		let len = this.snapshots.length;

		if (len > 2)
		{
			this.snapshots.splice(0, len-2);
		}
	}

	update(dt:number)
	{
		let len = this.snapshots.length;
		let snap = this.snapshots[len-1];
		if (snap == null)
			return;

		for (let i in snap.transforms)
		{
			let s = snap.transforms[i];

			let ent = Manager.entities[s.id];
			if (ent == null)
				continue;

			let tra = ent.getComponent(Transform);
			if (tra == null)
				continue;

			if (s.p != null)
			{
				tra.position.x = s.p.x;
				tra.position.y = s.p.y;
				tra.position.z = s.p.z;
			}

			if (s.s != null)
			{
				tra.scale.x = s.s.x;
				tra.scale.y = s.s.y;
				tra.scale.z = s.s.z;
			}

			if (s.r != null)
			{
				tra.rotation.x = s.r.x;
				tra.rotation.y = s.r.y;
				tra.rotation.z = s.r.z;
				tra.rotation.w = s.r.w;
			}
		}

		for (let i in snap.texts)
		{
			let s = snap.texts[i];
			if (s.val == null)
				continue;

			let ent = Manager.entities[s.id];
			if (ent == null)
				continue;

			let text = ent.getComponent(TextShape);
			if (text == null) continue;

			text.value = s.val;
		}

		for (let i in snap.audio)
		{
			let s = snap.audio[i];
			if (s.isPlay == false) continue;
			let ent = Manager.entities[s.id];
			if (ent == null)
				continue;

			ent.getComponent(AudioSource).playOnce();
			s.isPlay = false;
		}
	}
}
