import { SystemSnapshot } from "./systemSnapshot";
import { ClientWS } from "./clientWS"

export class Manager
{
    static ws:ClientWS;
    static sysSnapshot:SystemSnapshot;
    static entities = {};
}