import { Manager } from "./manager"

export class ClientWS
{
    static instance:ClientWS;

    ws;

    constructor()
    {
        if (ClientWS.instance == null)
            ClientWS.instance = this;
    }

    connect(url: string) {
        this.ws = new WebSocket(url);
        if (this.ws != null) {
            this.ws.onopen = (event) => { this.onOpen(event); };
            this.ws.onmessage = (event) => { this.onMessage(event); };
            this.ws.onerror = (event) => { this.onError(event); };
            this.ws.onclose = (event) => { this.onClose(event); };
        }
    }

onOpen(event) {
    log("onOpen", event);
}

onMessage(event) {
    if (event.data == null) return;

    let data = JSON.parse(event.data);

    if (data.method = "snap") {
        Manager.sysSnapshot.push(data);
        //log("got snapshot", data);
    }
}

onError(event) {
    log("onError", event);
}

onClose(event) {
    log("onClose", event);
}

send(message) {
	this.ws.send(message);
}

}
