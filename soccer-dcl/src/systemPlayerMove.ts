import { Manager } from "./manager"

var camera = Camera.instance;
var cameraPosPrev = new Vector3();
var cameraRotPrev = new Quaternion();

function didCameraChange()
{
	if (cameraPosPrev.x != camera.position.x || 
		cameraPosPrev.y != camera.position.y || 
		cameraPosPrev.z != camera.position.z)
	{
		return true;
	}

	if (cameraRotPrev.x != camera.rotation.x || 
		cameraRotPrev.y != camera.rotation.y || 
		cameraRotPrev.z != camera.rotation.z)
	{
		return true;
	}

	return false;
}

export class SystemPlayerMove implements ISystem 
{
	update() 
	{
		if (didCameraChange() == false)
			return;

		let pos = camera.position;
		let rot = camera.rotation;

		// Limit to 4 decimal places to save bytes
		let message = 
		{
			method:"playerMove",
			data:
			{
				position:
				{
					x:Number(pos.x.toFixed(4)),
					y:Number(pos.y.toFixed(4)),
					z:Number(pos.z.toFixed(4))
				},
				rotation:
				{
					x:Number(rot.x.toFixed(4)),
					y:Number(rot.y.toFixed(4)),
					z:Number(rot.z.toFixed(4)),
					w:Number(rot.z.toFixed(4))
				}
			}
		};

		cameraPosPrev = camera.position.clone();
		cameraRotPrev = camera.rotation.clone();

		// let msg = JSON.stringify(message);
		// console.log(message);
		// Manager.ws.send(msg);
	}
}
