import * as Loader from "./loader";
import { SystemSnapshot } from "./systemSnapshot";
import { Manager } from "./manager";
import { ClientWS } from "./clientWS";
import { SystemBillboard } from "./systemBillboard";
//import { SystemPlayerMove } from "./systemPlayerMove";

//const wsServer = "ws://br-prd.altitude-games.com:8888";
const wsServer = "ws://localhost:8888";

Loader.loadScene();

Manager.ws = new ClientWS();
Manager.ws.connect(wsServer);

Manager.sysSnapshot = new SystemSnapshot();
engine.addSystem(Manager.sysSnapshot);

engine.addSystem(new SystemBillboard());

