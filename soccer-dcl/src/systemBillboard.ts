import {Billboard} from "./billboard"

export class SystemBillboard implements ISystem
{
	update() 
	{
		let camera = Camera.instance;
		let up = new Vector3(0,1,0);

		const myGroup = engine.getComponentGroup(Transform, Billboard)
		for (let entity of myGroup.entities)
		{
			let t = entity.getComponent(Transform);
			let pos = camera.position.clone();
			pos.y = t.position.y;
			t.lookAt(pos, up);
		}
	}
}
