import * as Scene from "./scene"
import { Manager } from "./manager"
import { Billboard } from "./billboard"

var camera = Camera.instance;

export function sendPlayerClicked(obj:any, entity:Entity)
{
	let message = 
	{
		method:obj.evtName,
		data: 
		{ 
			id:entity["objId"], 
			param:obj.param,
			cPos:camera.position,
			cRot:camera.rotation
		}
	};

	Manager.ws.send(JSON.stringify(message));
}

export function loadComponent(obj:any, entity:Entity)
{
	//log("loadComponent", obj.tag);
	if (obj.tag == "BoxShape")
	{
		let shape = new BoxShape();
		entity.addComponent(shape);
		shape.withCollisions = obj.withCollisions;

		if (obj.material != null)
		{
			let id = obj.material;
			if (Manager.entities[id] != null)
			{
				let material = Manager.entities[id].getComponent(Material);
				entity.addComponent(material);
				//log("assign material to box", material);
			}
		}
	}
	else if (obj.tag == "GLTFShape")
	{
		let shape = new GLTFShape(obj.src);
		entity.addComponent(shape);
	}
	else if (obj.tag == "TextShape")
	{
		let shape = new TextShape(obj.src);
		entity.addComponent(shape);

		shape.value = obj.value;
		shape.color = Color3.FromHexString(obj.color);
		shape.fontSize = obj.fontSize;
		shape.lineCount = obj.lineCount;
		//log("added TextShape", shape);
	}
	else if (obj.tag == "OnClick")
	{
		let click = new OnClick(e => sendPlayerClicked(obj, entity) );
		entity.addComponent(click);
	}
	else if (obj.tag == "Billboard")
	{
		entity.addComponent(new Billboard());
		log("added Billboard component to", entity);
	}
	else if (obj.tag == "Material")
	{
		let material = new Material();
		material.albedoColor = Color3.FromHexString(obj.albedoColor);
		material.alpha = obj.alpha;
		material.hasAlpha = obj.hasAlpha;

		if (obj.albedoTexture != null)
		{
			material.albedoTexture = new Texture(obj.albedoTexture);
		}

		entity.addComponent(material);
		//log("loadMaterial", material);
	}
	else if (obj.tag == "AudioSource")
	{
		let clip = new AudioClip(obj.clip);
		let source = new AudioSource(clip);
		entity.addComponent(source);
	}
}

export function loadEntity(obj:any, p:Entity = null)
{
	let entity = new Entity(obj.name);
	entity["objId"] = obj.id;
	engine.addEntity(entity);
	Manager.entities[obj.id] = entity;

	if (p != null)
	{
		entity.setParent(p);
	}

	let transform = new Transform();
	if (obj.p != null)
	{
		let p = obj.p;
		transform.position = new Vector3(p.x, p.y, p.z);
	}

	if (obj.s != null)
	{
		let s = obj.s;
		transform.scale = new Vector3(s.x, s.y, s.z);
	}

	if (obj.r != null)
	{
		let r = obj.r;
		transform.rotation = new Quaternion(r.x, r.y, r.z, r.w);
	}

	entity.addComponent(transform);

	for (let comp of obj.components)
	{
		if (comp == null) continue;
		loadComponent(comp, entity);
	}

	for (let child of obj.children)
	{
		if (child == null) continue;
		loadEntity(child, entity);
	}
}

export function loadScene()
{
	let data = Scene.getSceneData();
	for (let obj of data.scene)
	{
		if (obj == null || obj.tag == null) continue;
		loadEntity(obj);
	}
}

