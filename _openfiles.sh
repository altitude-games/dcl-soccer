#!/bin/sh

ctags soccer-dcl/src/*.ts
ctags -a `find soccer-unity/Assets/Scripts/ -name *.cs`

vim soccer-dcl/src/*.ts \
	`find soccer-unity/Assets/Scripts/ -name *.cs`
